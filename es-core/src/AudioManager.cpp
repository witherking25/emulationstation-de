//  SPDX-License-Identifier: MIT
//
//  ES-DE
//  AudioManager.cpp
//
//  Low-level audio functions (using SDL2).
//

#include "AudioManager.h"

#include "Log.h"
#include "Settings.h"
#include "Sound.h"

#include <SDL2/SDL.h>
#include <SDL2/SDL_mixer.h>

AudioManager::AudioManager() noexcept
{
    // Init on construction.
    init();
}

AudioManager::~AudioManager()
{
    // Deinit on destruction.
    deinit();
}

AudioManager& AudioManager::getInstance()
{
    static AudioManager instance;
    return instance;
}

void AudioManager::init()
{
    LOG(LogInfo) << "Setting up AudioManager...";

    if (SDL_InitSubSystem(SDL_INIT_AUDIO) != 0) {
        LOG(LogError) << "Error initializing SDL audio!\n" << SDL_GetError();
        return;
    }

    LOG(LogInfo) << "Audio driver: " << SDL_GetCurrentAudioDriver();

    SDL_AudioSpec sRequestedAudioFormat;

    SDL_memset(&sRequestedAudioFormat, 0, sizeof(sRequestedAudioFormat));
    SDL_memset(&sAudioFormat, 0, sizeof(sAudioFormat));

    // Set up format and callback. SDL will negotiate these settings with the audio driver, so
    // if for instance the driver/hardware does not support 32-bit floating point output, 16-bit
    // integer may be selected instead. ES-DE will handle this automatically as there are no
    // hardcoded audio settings elsewhere in the code.
    sRequestedAudioFormat.freq = 44100;
    sRequestedAudioFormat.format = AUDIO_F32;
    sRequestedAudioFormat.channels = 2;
    sRequestedAudioFormat.samples = 1024;

    sAudioFormat = sRequestedAudioFormat;

    Mix_OpenAudioDevice(
        sRequestedAudioFormat.freq,
        sRequestedAudioFormat.format,
        sRequestedAudioFormat.channels,
        sRequestedAudioFormat.samples,
        0,
        SDL_AUDIO_ALLOW_FREQUENCY_CHANGE |
        SDL_AUDIO_ALLOW_CHANNELS_CHANGE |
        SDL_AUDIO_ALLOW_SAMPLES_CHANGE
        );
    update();
//    for (int i {0}; i < SDL_GetNumAudioDevices(0); ++i) {
//        LOG(LogInfo) << "Detected playback device: " << SDL_GetAudioDeviceName(i, 0);
//    }
//
//    sAudioDevice = SDL_OpenAudioDevice(0, 0, &sRequestedAudioFormat, &sAudioFormat,
//                                       SDL_AUDIO_ALLOW_ANY_CHANGE);
//
//    if (sAudioDevice == 0) {
//        LOG(LogError) << "Unable to open audio device: " << SDL_GetError();
//        sHasAudioDevice = false;
//    }
//
//    if (sAudioFormat.freq != sRequestedAudioFormat.freq) {
//        LOG(LogDebug) << "AudioManager::init(): Requested sample rate "
//                      << std::to_string(sRequestedAudioFormat.freq)
//                      << " could not be set, obtained " << std::to_string(sAudioFormat.freq);
//    }
//    if (sAudioFormat.format != sRequestedAudioFormat.format) {
//        LOG(LogDebug) << "AudioManager::init(): Requested format "
//                      << std::to_string(sRequestedAudioFormat.format)
//                      << " could not be set, obtained " << std::to_string(sAudioFormat.format);
//    }
//    if (sAudioFormat.channels != sRequestedAudioFormat.channels) {
//        LOG(LogDebug) << "AudioManager::init(): Requested channel count "
//                      << std::to_string(sRequestedAudioFormat.channels)
//                      << " could not be set, obtained " << std::to_string(sAudioFormat.channels);
//    }
//#if defined(_WIN64) || defined(__APPLE__)
//    // Beats me why the buffer size is not divided by the channel count on some operating systems.
//    if (sAudioFormat.samples != sRequestedAudioFormat.samples) {
//#else
//    if (sAudioFormat.samples != sRequestedAudioFormat.samples / sRequestedAudioFormat.channels) {
//#endif
//        LOG(LogDebug) << "AudioManager::init(): Requested sample buffer size "
//                      << std::to_string(sRequestedAudioFormat.samples /
//                                        sRequestedAudioFormat.channels)
//                      << " could not be set, obtained " << std::to_string(sAudioFormat.samples);
//    }





    // Just in case someone changed the es_settings.xml file manually to invalid values.
    if (Settings::getInstance()->getInt("SoundVolumeNavigation") > 100)
        Settings::getInstance()->setInt("SoundVolumeNavigation", 100);
    if (Settings::getInstance()->getInt("SoundVolumeNavigation") < 0)
        Settings::getInstance()->setInt("SoundVolumeNavigation", 0);
    if (Settings::getInstance()->getInt("SoundVolumeVideos") > 100)
        Settings::getInstance()->setInt("SoundVolumeVideos", 100);
    if (Settings::getInstance()->getInt("SoundVolumeVideos") < 0)
        Settings::getInstance()->setInt("SoundVolumeVideos", 0);

//    setupAudioStream(sRequestedAudioFormat.freq);
}

void AudioManager::deinit()
{
    Mix_CloseAudio();
}

void AudioManager::update()
{
    Mix_Volume(0, (int)((float)Settings::getInstance()->getInt("SoundVolumeNavigation") * 1.28f));
    Mix_Volume(1, (int)((float)Settings::getInstance()->getInt("SoundVolumeVideos") * 1.28f));
    Mix_VolumeMusic((int)((float)Settings::getInstance()->getInt("SoundVolumeMusic") * 1.28f));

    if (Settings::getInstance()->getBool("MusicSounds") && !Mix_PlayingMusic()) {
        MusicSounds::getInstance().playThemeMusicSound();
    }
    if (!Settings::getInstance()->getBool("MusicSounds") && Mix_PlayingMusic()) {
        MusicSounds::getInstance().stopThemeMusicSound();
    }
}

void AudioManager::processStream(const Uint8* samples, unsigned count)
{
    if (!sMuteStream) {

        SDL_AudioStream* sConversionStream =
            SDL_NewAudioStream(AUDIO_F32, 2, sAudioFormat.freq, sAudioFormat.format, sAudioFormat.channels,
                               sAudioFormat.freq);
        if (sConversionStream == nullptr) {
            LOG(LogError) << "Failed to create audio conversion stream:";
            LOG(LogError) << SDL_GetError();
            return;
        }
        if (SDL_AudioStreamPut(sConversionStream, samples, count * sizeof(Uint8)) == -1) {
            LOG(LogError) << "Failed to put samples in the conversion stream:";
            LOG(LogError) << SDL_GetError();
            return;
        }

        if (count > 0) {
            Uint8* converted {new Uint8[count * sizeof(Uint8)]};
            if (SDL_AudioStreamGet(sConversionStream, converted, count * sizeof(Uint8)) == -1) {
                LOG(LogError) << "Failed to convert video sound: " << SDL_GetError();
                SDL_FreeAudioStream(sConversionStream);
                delete[] converted;
                return;
            }
            Mix_PlayChannel(1, Mix_QuickLoad_RAW(converted, count * sizeof(Uint8)), 0);
        }
        SDL_FreeAudioStream(sConversionStream);
    }
}
