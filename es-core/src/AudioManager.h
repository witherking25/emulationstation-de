//  SPDX-License-Identifier: MIT
//
//  ES-DE
//  AudioManager.h
//
//  Low-level audio functions (using SDL2).
//

#ifndef ES_CORE_AUDIO_MANAGER_H
#define ES_CORE_AUDIO_MANAGER_H

#include <SDL2/SDL_audio.h>
#include <atomic>
#include <memory>
#include <vector>

class Sound;

class AudioManager
{
public:
    virtual ~AudioManager();
    static AudioManager& getInstance();

    void init();
    void deinit();

    void update();

    // Used for streaming audio from videos.
    void processStream(const Uint8* samples, unsigned count);

    void muteStream() { sMuteStream = true; }
    void unmuteStream() { sMuteStream = false; }

    bool getHasAudioDevice() { return sHasAudioDevice; }

    static inline SDL_AudioDeviceID sAudioDevice {0};
    static inline SDL_AudioSpec sAudioFormat;

private:
    AudioManager() noexcept;

    static inline std::atomic<bool> sMuteStream = false;
    static inline bool sHasAudioDevice = true;
};

#endif // ES_CORE_AUDIO_MANAGER_H
