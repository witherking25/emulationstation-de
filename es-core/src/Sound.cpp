//  SPDX-License-Identifier: MIT
//
//  ES-DE
//  Sound.cpp
//
//  Higher-level audio functions.
//  Navigation sounds, audio sample playback etc.
//

#include "Sound.h"

#include "AudioManager.h"
#include "Log.h"
#include "Settings.h"
#include "ThemeData.h"
#include "resources/ResourceManager.h"

std::shared_ptr<Sound> Sound::get(const std::string& path, Uint8 channel)
{
    auto it = sMap.find(path);
    if (it != sMap.cend())
        return it->second;

    std::shared_ptr<Sound> sound {std::shared_ptr<Sound>(new Sound(path, channel))};
    sMap[path] = sound;
    return sound;
}

std::shared_ptr<Sound> Sound::getFromTheme(ThemeData* const theme,
                                           const std::string& view,
                                           const std::string& element)
{
    std::string elemName {element.substr(6, std::string::npos)};

    if (theme == nullptr) {
        LOG(LogDebug) << "Sound::getFromTheme(): Using fallback sound file for \"" << elemName
                      << "\"";
        if (elemName == "music")
            return get("", 2);
        return get(ResourceManager::getInstance().getResourcePath(":/sounds/" + elemName + ".wav"));
    }

    LOG(LogDebug) << "Sound::getFromTheme(): Looking for tag <sound name=\"" << elemName << "\">";

    const ThemeData::ThemeElement* elem {theme->getElement(view, element, "sound")};
    if (!elem || !elem->has("path")) {
        LOG(LogDebug) << "Sound::getFromTheme(): Tag not found, using fallback sound file";
        if (elemName == "music")
            return get("", 2);
        return get(ResourceManager::getInstance().getResourcePath(":/sounds/" + elemName + ".wav"));
    }

    if (!Utils::FileSystem::exists(elem->get<std::string>("path"))) {
        LOG(LogError) << "Sound::getFromTheme(): Navigation sound tag found but sound file does "
                         "not exist, falling back to default sound";
        if (elemName == "music")
            return get("", 2);
        return get(ResourceManager::getInstance().getResourcePath(":/sounds/" + elemName + ".wav"));
    }

    LOG(LogDebug) << "Sound::getFromTheme(): Tag found, ready to load theme sound file";
    return get(elem->get<std::string>("path"), (elemName == "music" ? 2 : 0));
}

Sound::Sound(const std::string& path, const Uint8 channel)
    : mChannel(channel)
    , mPlaying(false)
{
    loadFile(path);
}

void Sound::loadFile(const std::string& path)
{
    mPath = path;
    init();
}

void Sound::init()
{
    mMusic = Mix_LoadMUS(mPath.c_str());
    mChunk = Mix_LoadWAV(mPath.c_str());
}

void Sound::deinit()
{
    stop();
    if (mMusic != nullptr) {
        Mix_FreeMusic(mMusic);
        mMusic = nullptr;
    }
    if (mChunk != nullptr) {
        Mix_FreeChunk(mChunk);
        mChunk = nullptr;
    }
    sMap.erase(mPath);
}

void Sound::play()
{
    if (mPath.empty())
        return;

    if (mChannel == 2) {
        if (!Settings::getInstance()->getBool("MusicSounds"))
            return;
    }
    else {
        if (!Settings::getInstance()->getBool("NavigationSounds"))
            return;
    }

    if (!AudioManager::getInstance().getHasAudioDevice())
        return;

    if (mChannel == 2)
        Mix_PlayMusic(mMusic, -1);
    else
        Mix_PlayChannel(mChannel, mChunk, 0);

    mPlaying = true;
}

void Sound::stop()
{
    // Flag our sample as not playing and rewind its position.
    mPlaying = false;
    if (mChannel == 2)
        Mix_HaltMusic();
    else
        Mix_HaltChannel(mChannel);
}

void Sound::fadeIn()
{
    if (mPath.empty())
        return;

    if (mChannel == 2) {
        if (!Settings::getInstance()->getBool("MusicSounds"))
            return;
    }
    else {
        if (!Settings::getInstance()->getBool("NavigationSounds"))
            return;
    }

    if (!AudioManager::getInstance().getHasAudioDevice())
        return;

    if (mChannel == 2)
        Mix_FadeInMusic(mMusic, -1, 300);
    else
        Mix_FadeInChannel(mChannel, mChunk, 0, 300);

    mPlaying = true;
}

void Sound::fadeOut()
{
    // Flag our sample as not playing and rewind its position.
    mPlaying = false;
    if (mChannel == 2)
        Mix_FadeOutMusic(300);
    else
        Mix_FadeOutChannel(mChannel, 300);
}


NavigationSounds& NavigationSounds::getInstance()
{
    static NavigationSounds instance;
    return instance;
}

void NavigationSounds::deinit()
{
    for (auto sound : mNavigationSounds) {
        sound->deinit();
    }
    mNavigationSounds.clear();
}

void NavigationSounds::loadThemeNavigationSounds(ThemeData* const theme)
{
    if (theme) {
        LOG(LogDebug) << "NavigationSounds::loadThemeNavigationSounds(): "
                         "Theme includes navigation sound support, loading custom sounds";
    }
    else {
        LOG(LogDebug) << "NavigationSounds::loadThemeNavigationSounds(): "
                         "Theme does not include navigation sound support, using fallback sounds";
    }

    mNavigationSounds.push_back(Sound::getFromTheme(theme, "all", "sound_systembrowse"));
    mNavigationSounds.push_back(Sound::getFromTheme(theme, "all", "sound_quicksysselect"));
    mNavigationSounds.push_back(Sound::getFromTheme(theme, "all", "sound_select"));
    mNavigationSounds.push_back(Sound::getFromTheme(theme, "all", "sound_back"));
    mNavigationSounds.push_back(Sound::getFromTheme(theme, "all", "sound_scroll"));
    mNavigationSounds.push_back(Sound::getFromTheme(theme, "all", "sound_favorite"));
    mNavigationSounds.push_back(Sound::getFromTheme(theme, "all", "sound_launch"));
}

void NavigationSounds::playThemeNavigationSound(NavigationSoundsID soundID)
{
    NavigationSounds::getInstance().mNavigationSounds[soundID]->play();
}

bool NavigationSounds::isPlayingThemeNavigationSound(NavigationSoundsID soundID)
{
    return NavigationSounds::getInstance().mNavigationSounds[soundID]->isPlaying();
}

MusicSounds& MusicSounds::getInstance()
{
    static MusicSounds instance;
    return instance;
}

void MusicSounds::deinit()
{
    if (mMusicSound) {
        mMusicSound->deinit();
        mMusicSound = nullptr;
    }
}

void MusicSounds::loadThemeMusicSound(ThemeData* const theme)
{
    mMusicSound = Sound::getFromTheme(theme, "all", "sound_music");
}

void MusicSounds::playThemeMusicSound()
{
    if (mMusicSound)
        mMusicSound->play();
}

void MusicSounds::stopThemeMusicSound()
{
    if (isPlayingThemeMusicSound())
        mMusicSound->stop();
}

void MusicSounds::fadeInThemeMusicSound()
{
    if (mMusicSound)
        mMusicSound->fadeIn();
}

void MusicSounds::fadeOutThemeMusicSound()
{
    if (isPlayingThemeMusicSound())
        mMusicSound->fadeOut();
}

bool MusicSounds::isPlayingThemeMusicSound()
{
    return mMusicSound && mMusicSound->isPlaying();
}
