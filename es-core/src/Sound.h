//  SPDX-License-Identifier: MIT
//
//  ES-DE
//  Sound.h
//
//  Higher-level audio functions.
//  Navigation sounds, audio sample playback etc.
//

#ifndef ES_CORE_SOUND_H
#define ES_CORE_SOUND_H

#include <SDL2/SDL_audio.h>
#include <SDL2/SDL_mixer.h>
#include <atomic>
#include <map>
#include <memory>
#include <sstream>
#include <string>
#include <vector>

class ThemeData;

class Sound
{
public:
    ~Sound() {}

    void init();
    void deinit();

    void loadFile(const std::string& path);

    void play();
    void stop();
    void fadeIn();
    void fadeOut();

    bool isPlaying() const { return mPlaying; }
    Uint8 getChannel() const { return mChannel; }
    std::string getPath() const { return mPath; }

    static std::shared_ptr<Sound> get(const std::string& path, Uint8 channel = 0);
    static std::shared_ptr<Sound> getFromTheme(ThemeData* const theme,
                                               const std::string& view,
                                               const std::string& elem);

private:
    Sound(const std::string& path = "", Uint8 channel = 0);

    static inline std::map<std::string, std::shared_ptr<Sound>> sMap;
    std::string mPath;
    Uint8 mChannel;
    Mix_Music* mMusic = nullptr;
    Mix_Chunk* mChunk = nullptr;
    std::atomic<bool> mPlaying;
};

enum NavigationSoundsID {
    SYSTEMBROWSESOUND,
    QUICKSYSSELECTSOUND,
    SELECTSOUND,
    BACKSOUND,
    SCROLLSOUND,
    FAVORITESOUND,
    LAUNCHSOUND
};

class NavigationSounds
{
public:
    static NavigationSounds& getInstance();

    void deinit();
    void loadThemeNavigationSounds(ThemeData* const theme);
    void playThemeNavigationSound(NavigationSoundsID soundID);
    bool isPlayingThemeNavigationSound(NavigationSoundsID soundID);

private:
    NavigationSounds() noexcept {};
    std::vector<std::shared_ptr<Sound>> mNavigationSounds;
};

class MusicSounds
{
public:
    static MusicSounds& getInstance();

    void deinit();
    void loadThemeMusicSound(ThemeData* const theme);
    void playThemeMusicSound();
    void stopThemeMusicSound();
    void fadeInThemeMusicSound();
    void fadeOutThemeMusicSound();
    bool isPlayingThemeMusicSound();

private:
    MusicSounds() noexcept {};
    std::shared_ptr<Sound> mMusicSound;
};

#endif // ES_CORE_SOUND_H
